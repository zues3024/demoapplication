package com.galvanize.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static com.galvanize.demo.DemoApplication.returnNumber;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void returnNumberShouldReturnOne() {
		int result = returnNumber();

		assertEquals(1, result);
	}

}
